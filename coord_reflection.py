from numpy import matrix
import math
from vector import vec
import gmpy2

class Ellipse:
    def __init__(self, a, b):
        if a < b:
            temp = a
            a = b
            b = temp
        self.a = a
        self.b = b
        self.f = math.sqrt(a * a - b * b)

    def get_x(self, t):
        return self.a * math.cos(t)

    def get_y(self, t):
        return self.b * math.sin(t)

    def get_vec(self, t):
        return vec(
            self.a * math.cos(t),
            self.b * math.sin(t),
            0
        )


class Sphere:
    def __init__(self, x, y, z, r):
        self.x = x
        self.y = y
        self.z = z
        self.r = r

    def get_x(self, theta, fi):
        return self.x + self.r * math.cos(theta) * math.sin(fi)

    def get_y(self, theta, fi):
        return self.y + self.r * math.sin(theta) * math.sin(fi)

    def get_z(self, theta, fi):
        return self.z + self.r * math.cos(fi)

    def get_vec(self, right_ascension, declination):
        declination = math.radians(90) - declination
        print("ra={}, dec={}".format(math.degrees(right_ascension), math.degrees(declination)))
        return vec(
            self.x + self.r * math.cos(right_ascension) * math.sin(declination),
            self.y + self.r * math.sin(right_ascension) * math.sin(declination),
            self.z + self.r * math.cos(declination)
        )


def mx_mul(mx, vector):
    result = [0, 0, 0]
    for col in range(0, 3):
        for row in range(0, 3):
            result[col] += mx[row][col] * vector[row]
    return result


def create_trafo_matrix(n):
    return [
        [1 - 2 * n.x * n.x, -2 * n.x * n.y, -2 * n.x * n.z],
        [- 2 * n.y * n.x, 1 - 2 * n.y * n.y, -2 * n.y * n.z],
        [- 2 * n.z * n.x, -2 * n.z * n.y, 1 - 2 * n.z * n.z],
    ]


def add_matrices(m1, m2):
    if len(m1) != len(m2) or len(m1[0]) != len(m2[0]):
        return None
    result = [[0 for col in range(len(m1[0]))] for row in range(len(m1))]
    for row_idx, row in enumerate(m1):
        for col_idx in range(len(row)):
            result[row_idx][col_idx] = m1[row_idx][col_idx] + m2[row_idx][col_idx]
    return result


def print_coord(title, coords_list):
    print("{}=({:.4f},{:.4f},{:.4f})".format(title, coords_list[0], coords_list[1], coords_list[2]))


v1 = vec(1, 0, 0)
v2 = vec(0, 1, 0)
v3 = vec(0, 0, 1)

print(v1.angle_deg(v2))
print(v1.angle_deg(v3))
print(v2.angle_deg(v1))
print(v2.angle_deg(v3))
print(v3.angle_deg(v1))
print(v3.angle_deg(v2))
print("---")

# n = Vector(0, -1, 0)
# d = Vector(0, 1, 0)
# print(d.reflect(n))

# Elliptic orbit path
ellipse = Ellipse(10, 20)
f = vec(ellipse.f, 0, 0)
print("focus=", f)

# Base station positions
sphere = Sphere(f.x, f.y, f.z, 1)
bs1_declination = math.radians(-2)
bs1_right_asc = math.radians(-8)
bs1 = sphere.get_vec(bs1_right_asc, bs1_declination) - f

b1 = vec(1,0,0).rotate_y(rad=bs1_declination).rotate_z(rad=bs1_right_asc)
b2 = vec(0,1,0).rotate_z(rad=bs1_right_asc)
b3 = b1.cross(b2)
print("{},{},{}".format(b1, b2, b3))
print("{},{},{},{},{},{}".format(
    b1.angle_deg(b2),
    b1.angle_deg(b3),
    b2.angle_deg(b1),
    b2.angle_deg(b3),
    b3.angle_deg(b1),
    b3.angle_deg(b2),
))

bs2_declination = math.radians(4)  # angle between Z and X
bs2_right_asc = math.radians(10)  # angle between X and Y, ascending to the east
bs2 = sphere.get_vec(bs2_right_asc, bs2_declination) - f
print("bs1={}".format(bs1))
print("bs2={}\n".format(bs2))

# Diff between two satellites in radians
diff_rad = math.radians(10)

for deg in range(0, 91, 15):
    print("===== degree {}° =====".format(deg))
    # Move the satellites
    t = math.radians(deg)
    # Calculate satellite current position
    # sat2 = [ellipse.get_x(diff_rad + t), ellipse.get_y(diff_rad + t), 0]
    # sat1 = [ellipse.get_x(t), ellipse.get_y(t), 0]
    sat2 = ellipse.get_vec(diff_rad + t) - f
    sat1 = ellipse.get_vec(t) - f
    # print("sat1=", sat1)
    # print("sat2=", sat2)

    dir1 = (sat1 - bs1).normalize()
    dir2 = (sat2 - sat1).normalize()
    dir3 = (bs2 - sat2).normalize()
    print("dir1=", dir1)
    # print("dir2=", dir2)
    # print("dir3=", dir3)

    dir1_decl = vec(dir1.x, dir1.y, 0).angle_rad(dir1)
    dir1_ra = vec(dir1.x, 0, dir1.z).angle_rad(dir1)
    print("declination={}, right_asc={}".format(math.degrees(dir1_decl), math.degrees(dir1_ra)))
    b1 = vec(1, 0, 0).rotate_y(rad=dir1_decl).rotate_z(rad=dir1_ra)
    b2 = vec(0, 1, 0).rotate_z(rad=dir1_ra)
    b3 = b1.cross(b2)
    print("{},{},{}".format(b1, b2, b3))
    print("{},{},{},{},{},{}".format(
        b1.angle_deg(b2),
        b1.angle_deg(b3),
        b2.angle_deg(b1),
        b2.angle_deg(b3),
        b3.angle_deg(b1),
        b3.angle_deg(b2),
    ))


    refl_angle = dir2.angle_deg(dir1 * -1)  # __\b/___
    # print("inAngle=", refl_angle, " deg")

    norm1 = ((dir1 * -1 + dir2) / 2).normalize()
    norm2 = ((dir2 * -1 + dir3) / 2).normalize()
    # print("norm1=", norm1)
    # print("norm2=", norm2)

    b1m = b1.reflect(norm1).reflect(norm2)
    b2m = b2.reflect(norm1).reflect(norm2)
    b3m = b3.reflect(norm1).reflect(norm2)
    print("{},{},{} -> {},{},{}".format(b1, b2, b3, b1m, b2m, b3m))
    print("delta_angle: {}, {}, {}°".format(b1.angle_deg(b1m), b2.angle_deg(b2m), b3.angle_deg(b3m)))
    print("delta_angle: {}, {}, {}°".format(b1.angle_deg(b1m*-1), b2.angle_deg(b2m*-1), b3.angle_deg(b3m*-1)))

    # print("angle1=", norm1.angle_deg(dir1 * -1), " deg")
    # print("angle2=", norm1.angle_deg(dir2), " deg")
    refl1 = dir1.reflect(norm1)
    refl2 = refl1.reflect(norm2)
    # print("refl1=", refl1)
    # print("refl2=", refl2)

    test = vec(0,0,1)
    test_r1 = test.reflect(norm1)
    test_r2 = test_r1.reflect(norm2)
    # print("test={}, mid={}, end={}".format(test, test_r1, test_r2))

    print("")


    # todo we have the 4 positions
    # calculate angle at every reflection -> calculate normal vector and reflect original ray
    # send probe vectors

    # calculate unit vectors pointing from satellites to the center of earth
    # sat1_n = vec(f[0] - sat1.x, f[1] - sat1.y, f[2] - sat1.z).normalize()
    # sat2_n = vec(f[0] - sat2.x, f[1] - sat2.y, f[2] - sat2.z).normalize()
    # print("sat1_n={}".format(sat1_n))
    # print("sat2_n={}".format(sat2_n))
    # print("")



# d = Vector(1, 1, 1)
# n1 = Vector(0, -1, 0)
# n1.normalize()
# T1 = matrix(create_trafo_matrix(n1))
#
#
# n2 = Vector(-1, 0, 0)
# n2.normalize()
# T2 = matrix(create_trafo_matrix(n2))
#
# r1 = d.reflect(n1)
# r2 = r1.reflect(n2)
#
# rt1 = matrix([d.x, d.y, d.z]) * T1
# rt2 = rt1 * T2

# print("{} reflects {} -> {}".format(d, n1, r1))
# print(matrix([d.x, d.y, d.z]) * T1)
# print("{} reflects {} -> {}".format(r1, n2, r2))
# print(matrix([r1.x, r1.y, r1.z]) * T2)
# print("original vector: {}".format(d))
# print("n1={}, n2={}".format(n1, n2))
# print("Normal reflection: {} and {}".format(r1.mx(), r2.mx()))
# print("Matrix reflection: {} and {}".format(rt1, rt2))
#
# n_super = Vector(n1.x + n2.x, n1.y+n2.y, n1.z+n2.z)
# n_super.normalize()
# Trafo = matrix(create_trafo_matrix(n_super))
# print("{}+{}={}".format(n1.mx(),n2.mx(),n_super))
# print(d.reflect(n_super))
# r_super = matrix([d.x, d.y, d.z]) * Trafo
# print("Concat normals:    {}".format(r_super))

# T = T1*T2
# r = matrix([d.x, d.y, d.z]) * T
# print(r)
#
# print("Inverse:")
# print(r * T.I)

# r1_iso = d.reflect(n2)
# print("{} reflects {} -> {}".format(d, n2, r1_iso))
# print("{} reflects {} -> {}".format(r1_iso, n1, r1_iso.reflect(n1)))

# n = Vector(n1.x + n2.x, n1.y + n2.y, n1.z + n2.z)
# print("Is it the same as {} reflects {} -> {} ??".format(d, n, d.reflect(n)))
