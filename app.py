import math
from vector import vec
import decimal_util

def triangle_elevation(of_side, sides):
    a, b, c = sides
    s = (a + b + c) / 2
    return 2 * ((s * (s - a) * (s - b) * (s - c)) ** .5) / of_side


def main():
    MAX_DEGREES = 52
    COLUMNS = 10
    MIN_HEIGHT = 100
    STEP_HEIGHT = 100
    HEIGHT_THRESHOLD = 100

    results = [[0 for col in range(COLUMNS - 1)] for row in range(MAX_DEGREES)]
    csv_results = [[0 for col in range(COLUMNS - 1)] for row in range(70)]
    r_earth = 6371  # km

    col_idx = 0
    max_height = MIN_HEIGHT + STEP_HEIGHT * (COLUMNS - 1)
    for h in range(MIN_HEIGHT, max_height, STEP_HEIGHT):
        # current distance of satellite from earth center
        r = r_earth + h
        results[0][col_idx] = (h, 0)
        csv_results[0][col_idx] = h

        # for angle in range(1, MAX_DEGREES):
        #     angle_rad = angle / 180.0 * math.pi
        #     cos_a = math.cos(angle_rad)
        #     a_b_cos = r * r * cos_a
        #     a2_b2 = r ** 2 + r ** 2
        #     distance = (a2_b2 - 2 * a_b_cos) ** 0.5
        #
        #     dist_elevation = triangle_elevation(distance, (r, r, distance))
        #     results[angle][col_idx] = (dist_elevation - r_earth, distance)
        #
        #     csv_results[angle][col_idx] = dist_elevation - r_earth

        for dist_idx in range(1, 35):
            distance = dist_idx * 200
            # csv_results[0][col_idx] = distance

            dist_elevation = triangle_elevation(distance, (r, r, distance))

            csv_results[dist_idx][col_idx] = dist_elevation - r_earth

        col_idx += 1

    with open("out.csv", "w") as f:
        # print("\ntop:    minimum height of the segment between satellites\nbottom: distance between satellites\n")
        # for angle, values in enumerate(results):
        #
        #     height_str = '\t'.join(
        #         "{:8.1f}".format(height) if height >= HEIGHT_THRESHOLD else '       -' for height, dist in values)
        #     dist_str = '\t'.join(
        #         "{:8.1f}".format(dist) if height >= HEIGHT_THRESHOLD else '       -' for height, dist in values)
        #     print("{:5}°\t{}\n     \t{}\n".format(angle, height_str, dist_str))

        for angle, row in enumerate(csv_results):
            values = ';'.join("{}".format(round(h)) for h in row)
            f.write("{};{}\n".format(angle*100, values))

            # for col_idx, tpl in enumerate(values):
            #     distance = tpl[1]
            #     g_height = results[0][col_idx][0]
            #     line_height = tpl[0]
                # f.write("{};{};{}\n".format(angle,
                #                             g_height,
                #                             tpl[0]))

                # print("Distances between mirror and generator")
                # for angle, values in enumerate(results):
                #     val_str = '\t'.join(
                #         "{:8.1f}".format(height, dist) if height >= HEIGHT_THRESHOLD else '       -' for height, dist in values)
                #     print("{:5}°\t{}".format(angle, val_str))


main()
