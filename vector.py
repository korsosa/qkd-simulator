import math
from math import cos, sin
import numpy

class vec:
    def __init__(self, x, y=0, z=0):
        if isinstance(x, vec):
            self.x = x.x
            self.y = x.y
            self.z = x.z
        else:
            self.x = x
            self.y = y
            self.z = z

    def normalize(self):
        length = self.length()
        if length == 0:
            return vec(0,0,0)
        return vec(self.x/length, self.y/length, self.z/length)

    def dot_product(self, other):
        return self.x * other.x + self.y * other.y + self.z * other.z

    def cross(self, other):
        Mx = [[self.y, self.z], [other.y, other.z]]
        My = [[self.x, self.z], [other.x, other.z]]
        Mz = [[self.x, self.y], [other.x, other.y]]
        det_x = numpy.linalg.det(Mx)
        det_y = numpy.linalg.det(My)
        det_z = numpy.linalg.det(Mz)
        return vec(det_x, -det_y, det_z)

    def length(self):
        return (self.x * self.x + self.y * self.y + self.z * self.z) ** 0.5

    def angle_rad(self, other):
        cos_theta = self.dot_product(other) / (self.length() * other.length())
        if cos_theta > 1:
            cos_theta = 1
        elif cos_theta < -1:
            cos_theta = -1
        return math.acos(cos_theta)

    def angle_deg(self, other):
        rad = self.angle_rad(other)
        return math.degrees(rad)

    def reflect(self, norm):
        if not isinstance(norm, vec):
            raise AttributeError
        dot_prod_2 = 2 * self.dot_product(norm)

        x = self.x - dot_prod_2 * norm.x
        y = self.y - dot_prod_2 * norm.y
        z = self.z - dot_prod_2 * norm.z
        return vec(x, y, z)

    def rotate_x(self, deg=None, rad=None):
        if deg is None and rad is None:
            raise AttributeError
        if deg is not None:
            rad = math.radians(deg)
        return self.__transform([
            [1, 0, 0],
            [0, cos(rad), -sin(rad)],
            [0, sin(rad), cos(rad)]
        ])

    def rotate_y(self, deg=None, rad=None):
        if deg is None and rad is None:
            raise AttributeError
        if deg is not None:
            rad = math.radians(deg)
        rad *= -1
        return self.__transform([
            [cos(rad), 0, sin(rad)],
            [0, 1, 0],
            [-sin(rad), 0, cos(rad)]
        ])

    def rotate_z(self, deg=None, rad=None):
        if deg is None and rad is None:
            raise AttributeError
        if deg is not None:
            rad = math.radians(deg)
        rad *= -1
        return self.__transform([
            [cos(rad), -sin(rad), 0],
            [sin(rad), cos(rad), 0],
            [0, 0, 1]
        ])

    def right_asc(self):
        proj = vec(self.x, 0, self.z)
        ra = proj.angle_rad(vec(1,0,0))
        if self.z > 0:
            ra *= -1
        return ra

    def declanation(self):
        proj = vec(self.x, 0, self.z)
        decl = proj.angle_rad(self)
        if self.y < 0:
            decl *= -1
        return decl

    def __transform(self, mx):
        if len(mx) != 3 or len(mx[0]) != 3 or len(mx[1]) != 3 or len(mx[2]) != 3:
            # not a 3x3 matrix
            raise TypeError
        return vec(
            self.x * mx[0][0] + self.y * mx[1][0] + self.z * mx[2][0],
            self.x * mx[0][1] + self.y * mx[1][1] + self.z * mx[2][1],
            self.x * mx[0][2] + self.y * mx[1][2] + self.z * mx[2][2],
        )

    def __sub__(self, other):
        """
        :rtype: vec
        """
        if isinstance(other, vec):
            return vec(self.x - other.x, self.y - other.y, self.z - other.z)
        else:
            return vec(self.x - other, self.y - other, self.z - other)

    def __add__(self, other):
        """
        :rtype: vec
        """
        if isinstance(other, vec):
            return vec(self.x + other.x, self.y + other.y, self.z + other.z)
        else:
            return vec(self.x + other, self.y + other, self.z + other)

    def __truediv__(self, other):
        """
        :rtype: vec
        """
        if isinstance(other, vec):
            return vec(self.x / other.x, self.y / other.y, self.z / other.z)
        else:
            return vec(self.x / other, self.y / other, self.z / other)

    def __mul__(self, other):
        """
        :rtype: vec
        """

        if isinstance(other, vec):
            raise TypeError
        else:
            return vec(self.x * other, self.y * other, self.z * other)

    def __str__(self):
        return "({:.6f},{:.6f},{:.6f})".format(self.x, self.y, self.z)