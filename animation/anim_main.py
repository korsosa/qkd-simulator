import math
from threading import Thread

import pyglet
import time

import sys
from pyglet.window import key
from pyglet.gl import glColor4f, glBegin, glEnd, glLineWidth, glVertex3f, GL_LINES, glBlendFunc, glEnable, glClearColor, \
    GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, glDepthFunc, GL_DEPTH_TEST, GL_LEQUAL, GL_BLEND, glTranslatef, gluSphere, \
    gluNewQuadric, gluPerspective, glLoadIdentity, glMatrixMode, GL_MODELVIEW, GL_PROJECTION, glOrtho, glRotatef, \
    glEnableClientState, glColorPointer, glVertexPointer, glDrawArrays, GL_VERTEX_ARRAY, GL_COLOR_ARRAY, GL_FLOAT, \
    GL_QUADS, glDisableClientState, glDisable, GLfloat, glViewport

from animation.shapes import EllipticOrbit, Earth, Satellite
from vector import vec

window = None
keys = key.KeyStateHandler()


def latitude_rad(degree):
    return math.radians(90 - degree % 360)


def longitude_rad(degree):
    degree %= 360
    if degree < 0:
        degree += 360
    return math.radians(-degree)


def draw_vector_at(pos, direction, color, line_width=1.0, scale=500):
    glColor4f(color[0], color[1], color[2], 1)
    glLineWidth(line_width)

    length = (direction.x ** 2 + direction.y ** 2 + direction.z ** 2) ** .5 / scale
    if length <= 0:
        return

    scaled = (direction.x / length, direction.y / length, direction.z / length)
    glBegin(GL_LINES)
    glVertex3f(pos.x, pos.y, pos.z)
    glVertex3f(pos.x + scaled[0], pos.y + scaled[1], pos.z + scaled[2])
    glEnd()


def opengl_init():
    """ Initial OpenGL configuration.
    """
    # background color
    glClearColor(1.0, 1.0, 1.0, 1.0)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    glEnable(GL_DEPTH_TEST)
    glDepthFunc(GL_LEQUAL)


min_height = 400  # km
# eccentricity determines the amount by which the orbit deviates from a perfect circle
# e = 0 - perfect circle
# 0 < e < 1 -> elliptical orbit
# e = 1 -> parabolic escape orbit
# 1 < e -> hyperbola
eccentricity = 0.2
R_periapsis = min_height + Earth.radius
semi_major_axis = R_periapsis / (1 - eccentricity)
# semi_major_axis = h + R_earth + f
focus_dist = semi_major_axis - min_height - Earth.radius

# f^2 = a^2 - b^2
# b = sqrt(a^2 - f^2)
semi_minor_axis = (semi_major_axis ** 2 - focus_dist ** 2) ** .5
orbit = EllipticOrbit.create(-focus_dist, 0, semi_major_axis, semi_minor_axis)
earth = Earth(0, 0)
earth.color = [0.8, 0.8, 0.8, 1]

# Base Station 1 [RED]
station_1 = Satellite.from_lat_lon(Earth.radius, latitude_rad(5), longitude_rad(5))
station_1.color = [0.8, 0.2, 0.2, 1]

station_2 = Satellite.from_lat_lon(Earth.radius, latitude_rad(-5), longitude_rad(-30))
station_2.color = [0.2, 0.8, 0.8, 1]


ang_between = 15
ang_center = -3

mirror_1 = Satellite(orbit.get_coordinates_parametric(math.radians(ang_center-ang_between)))
mirror_1.color = [216.0/255.0, 82.0/255.0, 82.0/255.0, 1]

generator = Satellite(orbit.get_coordinates_parametric(math.radians(ang_center)))
generator.color = [63.0/255.0, 71.0/255.0, 37.0/255.0, 1]

mirror_2 = Satellite(orbit.get_coordinates_parametric(math.radians(ang_center + ang_between)))
mirror_2.color = [66.0/255.0, 134.0/255.0, 244.0/255.0, 1]

base_g_m1 = [vec(0, 0, 0), vec(0, 0, 0), vec(0, 0, 0)]
base_g_m2 = [vec(0, 0, 0), vec(0, 0, 0), vec(0, 0, 0)]
base_m1_s1 = [vec(0, 0, 0), vec(0, 0, 0), vec(0, 0, 0)]
base_m2_s2 = [vec(0, 0, 0), vec(0, 0, 0), vec(0, 0, 0)]
mirror_1_normal = vec(0, 0, 0)
mirror_2_normal = vec(0, 0, 0)
los_m1s1_available = False
los_m2s2_available = False


def get_bases(direction):
    decl_bs1_m1 = vec(direction.x, 0, direction.z).angle_rad(direction)
    ra_bs1_m1 = vec(direction.x, direction.y, 0).angle_rad(direction)
    if direction.z < 0:
        ra_bs1_m1 *= -1
    if direction.y < 0:
        decl_bs1_m1 *= -1
    base1 = direction.normalize()
    base2 = vec(0, 1, 0).rotate_z(rad=base1.declanation()).rotate_y(rad=base1.right_asc())
    base3 = base1.cross(base2)
    return base1, base2, base3


def update_bases():
    global base_g_m1, base_g_m2, base_m1_s1, base_m2_s2, mirror_1_normal, mirror_2_normal

    # generator -> mirror direction vectors
    g_m1 = (mirror_1.pos - generator.pos).normalize()
    g_m2 = (mirror_2.pos - generator.pos).normalize()

    # mirror -> station direction vectors
    m1_s1 = (station_1.pos - mirror_1.pos).normalize()
    m2_s2 = (station_2.pos - mirror_2.pos).normalize()

    # mirror normal vectors
    mirror_1_normal = vec(m1_s1 + g_m1 * -1).normalize()
    mirror_2_normal = vec(m2_s2 + g_m2 * -1).normalize()

    # calculate original bases
    base_g_m1 = list(get_bases(g_m1))
    base_g_m2 = list(get_bases(g_m2))

    base_m1_s1_up = get_bases(m1_s1)[1]
    base_m2_s2_up = get_bases(m2_s2)[1]

    # calculate generator-mirror1 bases reflection
    base_m1_s1 = [
        vec(base_g_m1[0]).reflect(mirror_1_normal),  # z (green - direction)
        vec(base_g_m1[1]).reflect(mirror_1_normal),  # y (blue - up)
        vec(base_g_m1[2]).reflect(mirror_1_normal),  # x (red - right)
        base_m1_s1_up
    ]

    base_m2_s2 = [
        vec(base_g_m2[0]).reflect(mirror_2_normal),
        vec(base_g_m2[1]).reflect(mirror_2_normal),
        vec(base_g_m2[2]).reflect(mirror_2_normal),
        base_m2_s2_up
    ]



def is_los_available(station, mirror):
    return station.pos.angle_rad(mirror.pos - station.pos) < math.pi/3


def draw_the_universe():
    # Draw orbit and Earth
    orbit.draw()
    earth.draw()

    # Draw ground stations
    station_1.draw()
    station_2.draw()

    # Draw satellites
    mirror_1.draw(size_multiplier=0.5)
    mirror_2.draw(size_multiplier=0.5)
    generator.draw(size_multiplier=0.5)

    # Draw velocity vectors
    mirror_1.draw_velocity_vector()
    mirror_2.draw_velocity_vector()
    generator.draw_velocity_vector()

    # calc visibility
    if not los_m1s1_available or not los_m2s2_available:
        return

    # draw connection between objects
    generator.draw_line_to(mirror_1)
    generator.draw_line_to(mirror_2)
    mirror_1.draw_line_to(station_1)
    mirror_2.draw_line_to(station_2)

    # Draw normal vectors
    draw_vector_at(mirror_1.pos, mirror_1_normal, [0.4, 0.4, 0.4, 1])
    draw_vector_at(mirror_2.pos, mirror_2_normal, [0.4, 0.4, 0.4, 1])

    # draw original bases
    half_g_m1 = vec((generator.pos + mirror_1.pos) / 2)
    draw_vector_at(half_g_m1, base_g_m1[0], [0, 1, 0, 1.0])
    draw_vector_at(half_g_m1, base_g_m1[1], [0, 0, 1, 1.0])
    draw_vector_at(half_g_m1, base_g_m1[2], [1, 0, 0, 1.0])

    half_g_m2 = vec((generator.pos + mirror_2.pos) / 2)
    draw_vector_at(half_g_m2, base_g_m2[0], [0, 1, 0, 1.0])
    draw_vector_at(half_g_m2, base_g_m2[1], [0, 0, 1, 1.0])
    draw_vector_at(half_g_m2, base_g_m2[2], [1, 0, 0, 1.0])

    # draw reflected bases
    half_s1_m1 = vec((station_1.pos + mirror_1.pos) / 2)
    draw_vector_at(half_s1_m1, base_m1_s1[0], [0, 1, 0, 1.0])
    draw_vector_at(half_s1_m1, base_m1_s1[1], [0, 0, 1, 1.0])
    draw_vector_at(half_s1_m1, base_m1_s1[2], [1, 0, 0, 1.0])
    draw_vector_at(half_s1_m1, base_m1_s1[3], [0, 0, 0, 1.0])

    half_s2_m2 = vec((station_2.pos + mirror_2.pos) / 2)
    draw_vector_at(half_s2_m2, base_m2_s2[0], [0, 1, 0, 1.0])
    draw_vector_at(half_s2_m2, base_m2_s2[1], [0, 0, 1, 1.0])
    draw_vector_at(half_s2_m2, base_m2_s2[2], [1, 0, 0, 1.0])
    draw_vector_at(half_s2_m2, base_m2_s2[3], [0, 0, 0, 1.0])

    # print("Vg={:.4f} km/s".format(generator.velocity.length()))
    # print("Vm1={:.4f} km/s".format(mirror_1.velocity.length()))
    # print("Vm2={:.4f} km/s".format(mirror_2.velocity.length()))


move_objects = True


class camera(object):
    """ A camera.
    """
    mode = 3
    x, y, z = 0, 0, 5120 * 3
    rx, ry, rz = 15, -65, 0
    # w, h = 640, 480
    w, h = 1024, 768
    far = 81920
    fov = 60

    look_x, look_y, look_z = 0, 0, 0

    def view(self, width, height):
        """ Adjust window size.
        """
        self.w, self.h = width, height
        glViewport(0, 0, width, height)
        print("Viewport " + str(width) + "x" + str(height))
        if self.mode == 2:
            self.isometric()
        elif self.mode == 3:
            self.perspective()
        else:
            self.default()

    def default(self):
        """ Default pyglet projection.
        """
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(0, self.w, 0, self.h, -1, 1)
        glMatrixMode(GL_MODELVIEW)

    def isometric(self):
        """ Isometric projection.
        """
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glOrtho(-self.w / 2., self.w / 2., -self.h / 2., self.h / 2., 0, self.far)
        glMatrixMode(GL_MODELVIEW)

    def perspective(self):
        """ Perspective projection.
        """
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(self.fov, float(self.w) / self.h, 0.1, self.far)
        glMatrixMode(GL_MODELVIEW)

    def key(self, symbol, modifiers):
        global move_objects
        """ Key pressed event handler.
        """
        if symbol == key.F1:
            self.mode = 1
            self.default()
            print("Projection: Pyglet default")
        elif symbol == key.F2:
            print("Projection: 3D Isometric")
            self.mode = 2
            self.isometric()
        elif symbol == key.F3:
            print("Projection: 3D Perspective")
            self.mode = 3
            self.perspective()
        elif self.mode == 3 and (symbol == key.NUM_SUBTRACT or symbol == key.MINUS):
            self.fov -= 1
            self.perspective()
        elif self.mode == 3 and (symbol == key.NUM_ADD or symbol == key.EQUAL):
            self.fov += 1
            self.perspective()
        elif self.mode == 3 and (symbol == key.UP or symbol == key.DOWN):
            pass
            # rate = 3
            # delta = rate if symbol == key.UP else -rate
            # self.rx = ((180 + self.rx + delta) % 360) - 180
            # self.perspective()
        elif symbol == key.SPACE:
            move_objects = not move_objects
        else:
            print("KEY " + key.symbol_string(symbol))

    def drag(self, x, y, dx, dy, button, modifiers):
        """ Mouse drag event handler.
        """
        # print("drag: button={}".format(button))
        if button == 1:
            self.x -= dx * 2
            self.y -= dy * 2
        elif button == 2:
            self.x -= dx * 2
            self.z -= dy * 2
        elif button == 4:
            self.ry += dx / 4.
            self.rx -= dy / 4.

    def scroll(self, x, y, scroll_x, scroll_y):
        if scroll_y != 0 and self.mode == 3:
            multiplier = 1.05
            if scroll_y > 0:
                self.x *= multiplier
                self.y *= multiplier
                self.z *= multiplier
            else:
                self.x /= multiplier
                self.y /= multiplier
                self.z /= multiplier
            # self.fov += scroll_y
            # if self.fov < 30:
            #     self.fov = 30
            # elif self.fov > 148:
            #     self.fov = 148
            self.perspective()
            # print("scroll_y={}, fov={}".format(scroll_y, self.fov))

        if scroll_x != 0 and self.mode == 3:
            self.rx = ((180 + self.rx + scroll_x) % 360) - 180
            self.perspective()
            # print("scroll_x={}, r={}".format(scroll_y, self.rx))

    def look_at(self, x, y, z):
        eye = self.x, self.y, self.z
        center = x, y, z
        up = 0, 1, 0
        # gluLookAt()

    def apply(self):
        """ Apply camera transformation.
        """
        glLoadIdentity()
        if self.mode == 1: return

        # gluLookAt(
        #     self.x, self.y, self.z,
        #     4000, 0, 0,
        #     0, 1, 0
        # )

        glTranslatef(-self.x, -self.y, -self.z)
        glRotatef(self.rx, 1, 0, 0)
        glRotatef(self.ry, 0, 1, 0)
        glRotatef(self.rz, 0, 0, 1)
        eye = self.x, self.y, self.z
        # center = x, y, z
        up = 0, 1, 0


def x_array(list):
    """ Converts a list to GLFloat list.
    """
    return (GLfloat * len(list))(*list)


def axis(d=20000):
    """ Define vertices and colors for 3 planes
    """
    vertices, colors = [], []
    # XZ RED
    vertices.extend([-d, 0, -d, d, 0, -d, d, 0, d, -d, 0, d])
    for i in range(0, 4):
        colors.extend([1, 0, 0, 0.5])
    # YZ GREEN
    vertices.extend([0, -d, -d, 0, -d, d, 0, d, d, 0, d, -d])
    for i in range(0, 4):
        colors.extend([0, 1, 0, 0.5])
    # XY BLUE
    vertices.extend([-d, -d, 0, d, -d, 0, d, d, 0, -d, d, 0])
    for i in range(0, 4):
        colors.extend([0, 0, 1, 0.5])
    return x_array(vertices), x_array(colors)


AXIS_VERTICES, AXIS_COLORS = axis()


def draw_vertex_array(vertices, colors, mode=GL_LINES):
    """ Draw a vertex array.
    """
    glEnableClientState(GL_VERTEX_ARRAY)
    glEnableClientState(GL_COLOR_ARRAY)
    glColorPointer(4, GL_FLOAT, 0, colors)
    glVertexPointer(3, GL_FLOAT, 0, vertices)
    glDrawArrays(GL_QUADS, 0, int(len(vertices) / 3))
    glDisableClientState(GL_VERTEX_ARRAY)
    glDisableClientState(GL_COLOR_ARRAY)


def draw_axis():
    """ Draw the 3 planes
    """
    glEnable(GL_DEPTH_TEST)
    draw_vertex_array(AXIS_VERTICES, AXIS_COLORS, GL_QUADS)
    glDisable(GL_DEPTH_TEST)


class CameraWindow(pyglet.window.Window):
    def __init__(self):
        super(CameraWindow, self).__init__(camera.w, camera.h, resizable=True)
        opengl_init()
        self.cam = camera()
        self.on_resize = self.cam.view
        self.on_key_press = self.cam.key
        self.on_mouse_drag = self.cam.drag
        self.on_mouse_scroll = self.cam.scroll

    def on_draw(self):
        self.clear()
        self.cam.apply()
        # draw_axis()
        draw_the_universe()


last_check = 0

measure_start = 0


def curr_ts():
    return int(round(time.time() * 1000))

last_ang_s1, last_ang_s2 = None, None
def scheduled_callback(dt):
    global last_check, los_m1s1_available, los_m2s2_available, measure_start, last_ang_s1, last_ang_s2
    last_check += dt

    if move_objects:
        fast_dt = dt * 10
        steps = int(fast_dt) if fast_dt >= 1 else 1
        fine_dt_sec = fast_dt / steps
        # print("fast_dt={}".format(fast_dt))
        ang_velo_list = []
        for x in range(0, steps):
            # print("  {}".format(fine_dt_sec))
            mirror_1.move(orbit, fine_dt_sec)
            mirror_2.move(orbit, fine_dt_sec)
            generator.move(orbit, fine_dt_sec)

            los_m1s1_available = is_los_available(station_1, mirror_1)
            los_m2s2_available = is_los_available(station_2, mirror_2)
            if los_m1s1_available and los_m2s2_available:
                if measure_start == 0:
                    measure_start = curr_ts()
                orig_s1_base = base_m1_s1[1]
                orig_s2_base = base_m2_s2[1]
                update_bases()
                if orig_s1_base.length() > 0.0001 and orig_s2_base.length() > 0.0001:
                    s1_angle_diff = base_m1_s1[1].angle_rad(orig_s1_base)
                    s2_angle_diff = base_m2_s2[1].angle_rad(orig_s2_base)
                    ang_velo_s1 = s1_angle_diff / fine_dt_sec
                    ang_velo_s2 = s2_angle_diff / fine_dt_sec
                    # ang_velo_list.append((curr_ts() - measure_start, ang_velo_s1, ang_velo_s2))
                    # print("s1={} s2={}".format(ang_velo_s1, ang_velo_s2))
                    real_s1_angle_diff = base_m1_s1[1].angle_deg(base_m1_s1[3])
                    real_s2_angle_diff = base_m2_s2[1].angle_deg(base_m2_s2[3])
                    sign1 = '+' if base_m1_s1[1].dot_product(base_m1_s1[3]) >= 0 else '-'
                    sign2 = '+' if base_m2_s2[1].dot_product(base_m2_s2[3]) >= 0 else '-'
                    ang_velo_list.append((curr_ts() - measure_start, real_s1_angle_diff, real_s2_angle_diff))
                    print("s1={}{}° s2={}{}°".format(sign1,real_s1_angle_diff, sign2, real_s2_angle_diff))

        # print to file here
        # if len(ang_velo_list) > 0:
        #     with open("angular_velocity_6.txt", 'a+') as outfile:
        #         for time, s1, s2 in ang_velo_list:
        #             # output = "{};{};{}\n".format(time, s1, s2)
        #             if last_ang_s1 is not None and last_ang_s2 is not None:
        #                 output = "{};{};{}\n".format(time, (s1-last_ang_s1)/fine_dt_sec, (s2-last_ang_s2)/fine_dt_sec)
        #                 outfile.write(output)
        #                 print(output)
        #
        #             last_ang_s1 = s1
        #             last_ang_s2 = s2
            ang_velo_list = []

    # check pressed key controls
    if last_check >= 0.1:
        # print("scheduled: dt={}ms".format(last_check))
        if keys[key.UP] or keys[key.DOWN]:
            rate = 3
            delta = rate if keys[key.UP] else -rate
            window.cam.rx = ((180 + window.cam.rx + delta) % 360) - 180
            window.cam.perspective()
        elif keys[key.LEFT] or keys[key.RIGHT]:
            rate = 3
            delta = rate if keys[key.LEFT] else -rate
            window.cam.ry = ((180 + window.cam.ry + delta) % 360) - 180
            window.cam.perspective()
        last_check = 0


if __name__ == '__main__':
    print("OpenGL Projections")
    print("---------------------------------")
    print("Projection matrix -> F1, F2, F3")
    print("Camera -> Drag LMB, CMB, RMB")
    print("")

    window = CameraWindow()
    window.push_handlers(keys)

    # t = Thread(target=check_key_press, args=(window,), daemon=True)
    # t.start()

    pyglet.clock.schedule(scheduled_callback)

    pyglet.app.run()
