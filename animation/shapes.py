import math

import sys
from pyglet.gl import glColor4f, glLineWidth, glBegin, GL_LINE_LOOP, glVertex3f, glEnd, gluNewQuadric, gluSphere, \
    glPolygonMode, GL_FRONT_AND_BACK, GL_LINE, GL_FRONT, GL_BACK, glRotatef, GL_FILL, glTranslatef, GL_LINES

from vector import vec


class Earth:
    mu = 3.986e14
    mu_km = 3.986e5
    radius = 6371.0  # km

    def __init__(self, x0, y0):
        self.x0 = x0
        self.y0 = y0
        self.color = [0, 0, 0, 1]

    def set_color(self, r, g, b, a):
        self.color = [r, g, b, a]

    def draw(self):
        glRotatef(90, 1, 0, 0)
        wire_frame_div = 30
        sphere_div = 30

        # draw sphere
        glColor4f(self.color[0], self.color[1], self.color[2], self.color[3])
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        wire_frame = gluNewQuadric()
        gluSphere(wire_frame, Earth.radius, sphere_div, sphere_div)

        # draw wireframe
        glColor4f(self.color[0] - .2, self.color[1] - .2, self.color[2] - .2, self.color[3])
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)
        wire_frame = gluNewQuadric()
        # quadratic, radius, longitude_div, latitude_div
        gluSphere(wire_frame, Earth.radius + 10, wire_frame_div, wire_frame_div)

        # reset rotation and poly fill
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        glRotatef(-90, 1, 0, 0)


class EllipticOrbit:
    def __init__(self, x, y, semi_major_axis, semi_minor_axis, focus_distance, eccentricity):
        self.e = eccentricity  # e
        self.f = focus_distance  # f
        self.a = semi_major_axis  # b
        self.b = semi_minor_axis  # a
        self.x0 = x
        self.y0 = y

    @staticmethod
    def create(x, y, semi_major, semi_minor):
        f = EllipticOrbit.focus_distance(semi_major, semi_minor)
        e = EllipticOrbit.eccentricity_f(semi_major, f)
        return EllipticOrbit(x, y, semi_major, semi_minor, f, e)

    @staticmethod
    def focus_distance(semi_major_axis, semi_minor_axis):
        return (float(semi_major_axis) ** 2 - float(semi_minor_axis) ** 2) ** 0.5

    @staticmethod
    def eccentricity_f(semi_major_axis, focus_distance):
        return float(focus_distance) / float(semi_major_axis)

    @staticmethod
    def eccentricity_b(semi_major_axis, semi_minor_axis):
        return (1 - (float(semi_minor_axis) / float(semi_major_axis)) ** 2) ** .5

    def get_coordinates_parametric(self, t):
        x = self.x0 + self.a * math.cos(t)
        z = self.y0 + self.b * math.sin(t)
        return vec(x, 0, z)

    def get_center(self):
        return self.x0, self.y0

    def get_x(self, y):
        return self.a * (1.0 - (y / self.b) ** 2) ** .5

    def get_y(self, x):
        return self.b * (1.0 - (x / self.a) ** 2) ** .5

    def get_angle_rad(self, position):

        cosine_val = (position.x - self.x0) / self.a
        sine_val = (position.z - self.y0) / self.b
        # print("{} = acos({}-{}) / {}".format(cosine_val, position.x, self.x0, self.a))
        if cosine_val > 1:
            cosine_val = 2 - cosine_val
        elif cosine_val < -1:
            cosine_val = - 2 - cosine_val
        if sine_val > 1:
            sine_val = 2 - sine_val
        elif sine_val < -1:
            sine_val = -2 - sine_val

        result = math.acos(cosine_val)
        # result2 = math.asin(sine_val)
        if result == 0:
            result = math.asin(sine_val)

        # print("sin={} cos={}".format(result2, result))
        if position.z < 0:
            result = 2 * math.pi - result

        return result

    def get_tangent_rad(self, theta):
        denominator = math.sqrt(self.b ** 2 * math.cos(theta) ** 2 + self.a ** 2 * math.sin(theta) ** 2)
        return vec(
            -1 * (self.a * math.sin(theta)) / denominator,
            0,
            (self.b * math.cos(theta)) / denominator
        )

    def draw(self):
        # Elliptical orbit
        glColor4f(0, 0, 0, 0.4)
        glLineWidth(1.0)
        glBegin(GL_LINE_LOOP)

        for i in range(0, 360):
            rad = math.radians(i)
            pos = self.get_coordinates_parametric(rad)
            glVertex3f(pos.x, pos.y, pos.z)
            # glVertex3f(math.cos(rad) * 120, 0, math.sin(rad) * 200)
        glEnd()


class Satellite:
    def __init__(self, position):
        self.pos = position
        self.velocity = vec(0, 0, 0)
        self.color = [0, 0, 0, 1]

    @staticmethod
    def from_lat_lon(distance, lat_rad, lon_rad):
        x = distance * math.cos(lon_rad) * math.sin(lat_rad)
        y = 0 + distance * math.cos(lat_rad)
        z = 0 + distance * math.sin(lon_rad) * math.sin(lat_rad)
        return Satellite(vec(x, y, z))

    def set_color(self, red, green, blue, alpha=1.0):
        self.color = [red, green, blue, alpha]

    def recalculate_velocity(self, orbit: EllipticOrbit):
        theta = orbit.get_angle_rad(self.pos)
        dist = self.pos.length()
        tangent_dir = orbit.get_tangent_rad(theta)
        speed = math.sqrt(Earth.mu_km * (2.0 / dist - 1.0 / orbit.a))
        self.velocity = tangent_dir * speed

    def draw(self, size_multiplier=1):
        glColor4f(self.color[0], self.color[1], self.color[2], self.color[3])
        sphere = gluNewQuadric()
        pos = self.pos
        glTranslatef(pos.x, pos.y, pos.z)
        size = size_multiplier * (Earth.radius / 60)
        gluSphere(sphere, size, 40, 20)
        glTranslatef(-pos.x, -pos.y, -pos.z)

    def draw_velocity_vector(self):
        return
        glColor4f(0, 1, 0, 1)
        glLineWidth(2)

        length = self.velocity.length()
        if length <= 0:
            return

        # scaled = (self.velocity.x / length, direction.y / length, direction.z / length)
        scaled_velo = self.velocity * 50
        glBegin(GL_LINES)
        glVertex3f(self.pos.x, self.pos.y, self.pos.z)
        glVertex3f(self.pos.x + scaled_velo.x, self.pos.y + scaled_velo.y, self.pos.z + scaled_velo.z)
        glEnd()

    def draw_line_to(self, other):
        if not isinstance(other, Satellite):
            return
        shade = 0.1
        glColor4f(shade, shade, shade, 0.3)
        glLineWidth(2.0)
        glBegin(GL_LINES)
        glVertex3f(self.pos.x, self.pos.y, self.pos.z)
        glVertex3f(other.pos.x, other.pos.y, other.pos.z)
        glEnd()

    def move(self, orbit, dt_sec):
        dist = self.velocity * dt_sec
        self.pos += dist
        self.recalculate_velocity(orbit)
