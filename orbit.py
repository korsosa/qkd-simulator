from numpy import matrix, arange
import math

mu = 3.986e14  # standard grav parameter, m^3*s^-2

a = 10.0
b = 5.0

e = (1 - (b / a) ** 2) ** .5


def get_x(t):
    return a * math.cos(t)


def get_y(t):
    return b * math.sin(t)


def tangent(x,y):
    t_x = -y / (b * b)
    t_y = x / (a * a)
    length = math.sqrt(t_x * t_x + t_y * t_y)
    return [t_x / length, t_y / length]


def normal(x, y):
    n_x = x / (a * a)
    n_y = y / (b * b)
    length = math.sqrt(n_x * n_x + n_y * n_y)
    return [n_x / length, n_y / length]


def dot_prod(v1, v2):
    if len(v1) == 2:
        return v1[0] * v2[0] + v1[1] * v2[1]
    elif len(v1) == 3:
        return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]


def v_length(v):
    if len(v) == 2:
        return math.sqrt(v[0] * v[0] + v[1] * v[1])
    elif len(v) == 3:
        return math.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2])


def reflect(v, norm):
    dot = dot_prod(v, norm)
    r_x = v[0] - 2 * dot * norm[0]
    r_y = v[1] - 2 * dot * norm[1]
    r_z = v[2] - 2 * dot * norm[2]
    return [r_x, r_y, r_z]


def rotate_clockwise(v):
    return [v[1], -v[0], v[2]]


def rotate_anticlockwise(v):
    return [-v[1], v[0], v[2]]


# print(math.pi)
last_normal = None

base = [-4, 0, 1]
for i in arange(0, 2 * math.pi, 0.05):
    p = [get_x(i), get_y(i), 0]
    t = tangent(p[0], p[1])
    t.append(0)
    n = normal(p[0], p[1])
    n.append(0)

    result = 'P=({:.4f},{:.4f},{:.4f})'.format(p[0], p[1], p[2])
    # result += ' tangent:({:.4f},{:.4f})'.format(t[0], t[1])
    result += '\nN=({:.4f},{:.4f},{:.4f})'.format(n[0], n[1], n[2])

    if last_normal is not None:
        last_length = math.sqrt(last_normal[0] * last_normal[0] + last_normal[1] * last_normal[1])
        length = math.sqrt(n[0] * n[0] + n[1] * n[1])
        dot = dot_prod(last_normal, n)
        angle_rad = math.acos(dot / (last_length * length))
        result += '\ndiff: {}'.format(angle_rad)

    print(result)

    # calculate reflections
    ray = [p[0] - base[0], p[1] - base[1], p[2] - base[2]]
    ray = [ray[0]/v_length(ray), ray[1]/v_length(ray), ray[2]/v_length(ray)]
    ray2 = rotate_clockwise(ray)
    reflection = reflect(ray, n)
    reflection2 = reflect(ray2, n)

    print('({:.6f},{:.6f},{:.6f}) -> ({:.6f},{:.6f},{:.6f})'.format(ray[0], ray[1], ray[2], reflection[0], reflection[1], reflection[2]))
    print('({:.6f},{:.6f},{:.6f}) -> ({:.6f},{:.6f},{:.6f})'.format(ray2[0], ray2[1], ray2[2], reflection2[0], reflection2[1], reflection2[2]))
    print('\n')
    last_normal = n
